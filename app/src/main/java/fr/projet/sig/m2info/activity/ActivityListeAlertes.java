package fr.projet.sig.m2info.activity;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.adapter.AdapterAlerte;
import fr.projet.sig.m2info.adapter.AdapterAlerteDetails;
import fr.projet.sig.m2info.item.AlerteDetailsItem;
import fr.projet.sig.m2info.item.AlerteItem;
import fr.projet.sig.m2info.model.Alertearret;
import fr.projet.sig.m2info.model.Alerteligne;
import fr.projet.sig.m2info.model.Alerteparc;
import fr.projet.sig.m2info.model.Alerteveloplus;
import fr.projet.sig.m2info.model.Arret;
import fr.projet.sig.m2info.model.Ligne;
import fr.projet.sig.m2info.model.Parcvelo;
import fr.projet.sig.m2info.model.Stationveloplus;

public class ActivityListeAlertes extends AppCompatActivity {

    private AsyncTask<Object, Void, Void> inBackground;
    RecyclerView recyclerView;
    ArrayList<Ligne> lignes;
    ArrayList<Arret> arrets;
    ArrayList<Parcvelo> parcs;
    ArrayList<Stationveloplus> veloP;
    HashMap<String, Ligne> lignesMap;
    HashMap<String, Arret> arretsMap;
    HashMap<String, Stationveloplus> velosPMap;
    HashMap<String, Parcvelo> parcsMap;
    int radioChecked;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_liste_alertes);
        inBackground = new RunInBackground();
        inBackground.execute();
    }


    public void showDetails(String value) {
        ArrayList<AlerteDetailsItem> items = new ArrayList<>();
        switch (radioChecked) {
            case 0:
                Ligne ligne = lignesMap.get(value);
                for (Alerteligne al : ligne.getAlertelignes())
                    items.add(new AlerteDetailsItem(al.getDate(), al.getTextalert()));
                break;
            case 1:
                Arret arret = arretsMap.get(value);
                for (Alertearret aa : arret.getAlertearrets())
                    items.add(new AlerteDetailsItem(aa.getDate(), aa.getTextalert()));
                break;
            case 2:
                Parcvelo parc = parcsMap.get(value);
                for (Alerteparc ap : parc.getAlerteparcs())
                    items.add(new AlerteDetailsItem(ap.getDate(), ap.getTextalert()));
                break;
            case 3:
                Stationveloplus stationveloplus = velosPMap.get(value);
                for (Alerteveloplus av : stationveloplus.getAlerteveloplus())
                    items.add(new AlerteDetailsItem(av.getDate(), av.getTextalert()));
                break;
        }
        showDetails(items, value);
    }

    private void showDetails(ArrayList<AlerteDetailsItem> items, String info) {
        AdapterAlerteDetails adapter = new AdapterAlerteDetails(items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.popup_alerte, null);
        TextView tv_info = dialogView.findViewById(R.id.tv_info);
        tv_info.setText(info);
        RecyclerView rv_alertes = dialogView.findViewById(R.id.rv_details);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rv_alertes.setLayoutManager(layoutManager);
        rv_alertes.setAdapter(adapter);
        builder.setCancelable(true);
        builder.setView(dialogView);
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public void onRadioClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        ArrayList<AlerteItem> alerteItems = new ArrayList<>();
        switch (view.getId()) {
            case R.id.radio_ligne:
                if (checked) {
                    radioChecked = 0;
                    for(Ligne l:lignesMap.values())
                        alerteItems.add(new AlerteItem(l.getNumligne(),l.getAlertelignes().size(), true));
                }
                break;
            case R.id.radio_arret:
                if (checked) {
                    radioChecked = 1;
                    for(Arret a: arretsMap.values())
                        alerteItems.add(new AlerteItem(a.getIdarret(),a.getNomarret(),a.getAlertearrets().size(), false));
                }
                break;
            case R.id.radio_parcrealai:
                if (checked) {
                    radioChecked = 2;
                    for(Parcvelo p:parcsMap.values())
                        alerteItems.add(new AlerteItem(p.getIdparcv(),p.getNomarretparc(),p.getAlerteparcs().size(), false));
                }
                break;
            case R.id.radio_velop:
                if (checked){
                    radioChecked = 3;
                    for(Stationveloplus s:velosPMap.values()) {
                        alerteItems.add(new AlerteItem(s.getIdstationplus(), s.getNomarret(), s.getAlerteveloplus().size(), false));
                    }
                }
                break;
        }
        recyclerView.setAdapter(new AdapterAlerte(alerteItems, ActivityListeAlertes.this));
    }




    private class RunInBackground extends AsyncTask<Object, Void, Void> {

        private String readStream(InputStream is) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }
        @Override
        protected Void doInBackground(Object... objects) {
            SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
            String ip = sharedPreferences.getString("ip","");


            String urlStringLignes = "http://"+ip+":8081/transport/lignes";
            String urlStringArrets = "http://"+ip+":8081/transport/arrets";
            String urlStringVeloP = "http://"+ip+":8081/cyclable/stationsplus";
            String urlStringParc = "http://"+ip+":8081/cyclable/parcs";

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();

            String resultLignes = getResult(urlStringLignes);
            String resultArrets = getResult(urlStringArrets);
            String resultVeloP = getResult(urlStringVeloP);
            String resultParc = getResult(urlStringParc);
            if(resultLignes == null || resultArrets == null || resultVeloP == null || resultParc == null)
                return null;
            Type type = new TypeToken<ArrayList<Ligne>>(){}.getType();
            lignes = gson.fromJson(resultLignes, type);

            type = new TypeToken<ArrayList<Arret>>(){}.getType();
            arrets = gson.fromJson(resultArrets, type);

            type = new TypeToken<ArrayList<Parcvelo>>(){}.getType();
            parcs = gson.fromJson(resultParc, type);

            type = new TypeToken<ArrayList<Stationveloplus>>(){}.getType();
            veloP = gson.fromJson(resultVeloP, type);
            return null;
        }

        @Override
        protected  void onPostExecute(Void unused) {
            if (lignes == null || arrets == null || parcs == null || veloP == null)
                return;
            recyclerView = findViewById(R.id.rv_alertes);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);

            lignesMap = new HashMap<>();
            arretsMap = new HashMap<>();
            parcsMap = new HashMap<>();
            velosPMap = new HashMap<>();
            for (Ligne l : lignes) {
                int nbAlertes = l.getAlertelignes().size();
                if (nbAlertes > 0) {
                    lignesMap.put(l.getNumligne(), l);
                }
            }
            for (Arret a : arrets) {
                int nbAlertes = a.getAlertearrets().size();
                if (nbAlertes > 0) {
                    arretsMap.put(a.getNomarret(), a);
                }
            }
            for (Parcvelo p : parcs) {
                int nbAlertes = p.getAlerteparcs().size();
                if (nbAlertes > 0) {
                    parcsMap.put(p.getNomarretparc(), p);
                }
            }
            for (Stationveloplus s : veloP) {
                int nbAlertes = s.getAlerteveloplus().size();
                if (nbAlertes > 0) {
                    velosPMap.put(s.getNomarret(), s);
                }
            }
            findViewById(R.id.radio_ligne).performClick();
        }

        private String getResult(String urlString) {
            String result = null;
            try {
                URL url = new URL(urlString);
                HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(httpConnection .getInputStream()); // Stream
                result = readStream(in); // Read stream
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }
}
