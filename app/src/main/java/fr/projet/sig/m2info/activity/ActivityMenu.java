package fr.projet.sig.m2info.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import fr.projet.sig.m2info.R;

public class ActivityMenu extends AppCompatActivity implements LocationListener {

    protected LocationManager locationManager;
    Double lon,lat;

    SharedPreferences sharedPreferences;
    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activiy_menu);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(this, permissions, 42);
            return;
        }
        setTextView();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }


    private void setTextView() {
        TextView tv_option = findViewById(R.id.tv_option);
        TextView tv_alertes = findViewById(R.id.tv_alerte_tao);
        TextView tv_creer_alerte = findViewById(R.id.tv_creer_alerte);
        TextView tv_ligneA = findViewById(R.id.tv_ligneA);
        TextView tv_ligneB = findViewById(R.id.tv_ligneB);
        TextView tv_lignes = findViewById(R.id.tv_lignes_transport);
        TextView tv_arrets = findViewById(R.id.tv_liste_arrets);
        TextView tv_parcs = findViewById(R.id.tv_parc_relai);
        TextView tv_velop = findViewById(R.id.tv_velo_plus);
        TextView tv_ousuije = findViewById(R.id.tv_afficher_map);
        TextView tv_map_lignes = findViewById(R.id.tv_map_ligne);
        TextView tv_map_cyclable = findViewById(R.id.tv_map_pistes);


        tv_ousuije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMenu.this, ActivityMap.class);
                intent.putExtra("id",0);
                intent.putExtra("lat", lat);
                intent.putExtra("long",lon);
                startActivity(intent);
            }
        });

        tv_map_cyclable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMenu.this, ActivityMap.class);
                intent.putExtra("id",1);
                intent.putExtra("lat", lat);
                intent.putExtra("long",lon);
                startActivity(intent);
            }
        });

        tv_map_lignes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMenu.this, ActivityMap.class);
                intent.putExtra("id",2);
                intent.putExtra("lat", lat);
                intent.putExtra("long",lon);
                startActivity(intent);
            }
        });

        tv_ligneA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMenu.this, ActivityLigne.class);
                intent.putExtra("numLigne", "TA");
                startActivity(intent);
            }
        });

        tv_ligneB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMenu.this, ActivityLigne.class);
                intent.putExtra("numLigne", "TB");
                startActivity(intent);
            }
        });

        tv_lignes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMenu.this, ActivityListeBus.class));
            }
        });

        tv_arrets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMenu.this, ActivityListeArrets.class));
            }
        });

        tv_parcs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMenu.this,ActivityListeParcRelai.class));
            }
        });

        tv_velop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMenu.this, ActivityListeVelosP.class));
            }
        });

        tv_alertes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMenu.this, ActivityListeAlertes.class));
            }
        });

        tv_creer_alerte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMenu.this, ActivityNewAlerte.class));
            }
        });

        tv_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMenu.this, AcitivityOption.class));
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // permission was granted, yay! Do the
            // location-related task you need to do.
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Request location updates:
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                setTextView();
                return;
            }
        }
        finish();
    }

    @Override
    public void onLocationChanged(Location location) {
        lon = location.getLongitude();
        lat = location.getLatitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
