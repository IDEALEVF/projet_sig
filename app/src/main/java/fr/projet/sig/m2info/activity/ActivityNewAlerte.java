package fr.projet.sig.m2info.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.xml.validation.Validator;

import androidx.appcompat.app.AppCompatActivity;
import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.model.Arret;
import fr.projet.sig.m2info.model.Ligne;
import fr.projet.sig.m2info.model.Parcvelo;
import fr.projet.sig.m2info.model.Stationveloplus;

public class ActivityNewAlerte extends AppCompatActivity {

    private AsyncTask<Object, Void, Void> inBackground;
    private AsyncTask<String, Void, Void> sendData;
    private int idRadio = -1;
    Ligne l;
    Arret a;
    Stationveloplus s;
    Parcvelo p;

    ArrayList<Ligne> lignes;
    ArrayList<Arret> arrets;
    ArrayList<Parcvelo> parcs;
    ArrayList<Stationveloplus> veloP;
    HashMap<String, Ligne> lignesMap;
    HashMap<String, Arret> arretsMap;
    HashMap<String, Stationveloplus> velosPMap;
    HashMap<String, Parcvelo> parcsMap;

    Spinner spinner;
    private EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_alerte);
        inBackground = new RunInBackground();
        inBackground.execute();
    }

    public void onRadioClick(View view) {
        ArrayList<String> liste = new ArrayList<>();
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.radio_ligne:
                if (checked) {
                    for(Ligne l1:lignesMap.values()) {
                        liste.add(l1.getNumligne());
                    }
                }
                break;
            case R.id.radio_arret:
                if (checked) {
                    for(Arret a1: arretsMap.values()) {
                        liste.add(a1.getNomarret());
                    }
                }
                break;
            case R.id.radio_parcrealai:
                if (checked) {
                    for(Parcvelo p1:parcsMap.values())
                        liste.add(p1.getNomarretparc());
                }
                break;
            case R.id.radio_velop:
                if (checked){
                    for(Stationveloplus s:velosPMap.values())
                        liste.add(s.getNomarret());
                }
                break;
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, liste);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    private class RunInBackground extends AsyncTask<Object, Void, Void> {

        private String readStream(InputStream is) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }
        @Override
        protected Void doInBackground(Object... objects) {
            SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
            Intent intent = getIntent();
            int id = intent.getIntExtra("idRadio",-1);
            if(id != -1) {
                alerteUnique(id, intent, sharedPreferences);
                return null;
            }
            String ip = sharedPreferences.getString("ip","");
            String urlStringLignes = "http://"+ip+":8081/transport/lignes";
            String urlStringArrets = "http://"+ip+":8081/transport/arrets";
            String urlStringVeloP = "http://"+ip+":8081/cyclable/stationsplus";
            String urlStringParc = "http://"+ip+":8081/cyclable/parcs";

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();

            String resultLignes = getResult(urlStringLignes);
            String resultArrets = getResult(urlStringArrets);
            String resultVeloP = getResult(urlStringVeloP);
            String resultParc = getResult(urlStringParc);
            if(resultLignes == null || resultArrets == null || resultVeloP == null || resultParc == null)
                return null;
            Type type = new TypeToken<ArrayList<Ligne>>(){}.getType();
            lignes = gson.fromJson(resultLignes, type);

            type = new TypeToken<ArrayList<Arret>>(){}.getType();
            arrets = gson.fromJson(resultArrets, type);

            type = new TypeToken<ArrayList<Parcvelo>>(){}.getType();
            parcs = gson.fromJson(resultParc, type);

            type = new TypeToken<ArrayList<Stationveloplus>>(){}.getType();
            veloP = gson.fromJson(resultVeloP, type);
            return null;
        }


        public void alerteUnique(int idR, Intent intent, SharedPreferences sharedPreferences) {
            String ip = sharedPreferences.getString("ip","");
            idRadio = idR;
            String url="";
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();
            String result;
            Long id;
            switch (idRadio) {
                case 0:
                    String numLigne = intent.getStringExtra("numLigne");
                    url = "http://"+ip+":8081/transport/ligne?numLigne="+numLigne;
                    result = getResult(url);
                    if(result != null)
                        l= gson.fromJson(result, Ligne.class);
                    break;
                case  1:
                    id = intent.getLongExtra("id", -1);
                    url = "http://"+ip+":8081/transport/arret?idArret="+id;
                    result = getResult(url);
                    if(result != null)
                        a = gson.fromJson(result, Arret.class);
                    break;
                case 2:
                    id = intent.getLongExtra("id", -1);
                    url = "http://"+ip+":8081/cyclable/stationplus?idStationplus="+id;
                    result = getResult(url);
                    if(result != null)
                        s = gson.fromJson(result, Stationveloplus.class);
                    break;
                case 3:
                    id = intent.getLongExtra("id", -1);
                    url ="http://"+ip+":8081/cyclable/parc?idParc="+id;
                    result = getResult(url);
                    if(result != null)
                        p = gson.fromJson(result, Parcvelo.class);
                    break;
            }
        }

        @Override
        protected  void onPostExecute(Void unused) {
            spinner = findViewById(R.id.spinner);
            final RadioButton radiol = findViewById(R.id.radio_ligne);
            final RadioButton radioa = findViewById(R.id.radio_arret);
            final RadioButton radios = findViewById(R.id.radio_velop);
            final RadioButton radiop = findViewById(R.id.radio_parcrealai);
            if(idRadio != -1) {
                if(a == null && l == null && s == null && p == null ) {
                    return;
                }
                radiol.setEnabled(false);
                radioa.setEnabled(false);
                radios.setEnabled(false);
                radiop.setEnabled(false);
                ArrayList<String> liste = new ArrayList<>();
                switch (idRadio) {
                    case 0:
                        radiol.setChecked(true);
                        liste.add(l.getNumligne());
                        break;
                    case 1:
                        radioa.setChecked(true);
                        liste.add(a.getNomarret());
                        break;
                    case 2:
                        radios.setChecked(true);
                        liste.add(s.getNomarret());
                        break;
                    case 3:
                        radiop.setChecked(true);
                        liste.add(p.getNomarretparc());
                        break;
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, liste);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(dataAdapter);
                spinner.setEnabled(false);
            }
            else {
                if (lignes == null || arrets == null || parcs == null || veloP == null) {
                    return;
                }
                lignesMap = new HashMap<>();
                arretsMap = new HashMap<>();
                parcsMap = new HashMap<>();
                velosPMap = new HashMap<>();
                for (Ligne l1 : lignes) {
                    lignesMap.put(l1.getNumligne(), l1);
                }
                for (Arret a1 : arrets) {
                    arretsMap.put(a1.getNomarret(), a1);
                }
                for (Parcvelo p1 : parcs) {
                    parcsMap.put(p1.getNomarretparc(), p1);
                }
                for (Stationveloplus s1 : veloP) {
                    velosPMap.put(s1.getNomarret(), s1);
                }

                radiol.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRadioClick(v);
                    }
                });
                radioa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRadioClick(v);
                    }
                });
                radios.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRadioClick(v);
                    }
                });
                radiop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRadioClick(v);
                    }
                });
                radiol.performClick();
            }
            editText = findViewById(R.id.et_details);
            Button btn_valider = findViewById(R.id.btn_valider);
            Button btn_annuler = findViewById(R.id.btn_annuler);
            btn_valider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String detail = editText.getText().toString();
                    if(detail.length() < 1) {
                        return;
                    }
                    SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
                    String ip = sharedPreferences.getString("ip", "");
                    String urlString = "http://"+ip+":8081/alerte/";
                    JSONObject postData = new JSONObject();
                    JSONObject postId = new JSONObject();
                    sendData = new SendData();
                    String value = spinner.getSelectedItem().toString();
                    try {
                        postData.put("titrealerte", "null");
                        postData.put("type", "null");
                        postData.put("textalert", detail);
                        postData.put("cptresolu", 0);
                        if (radiol.isChecked()) {
                            if(l == null)
                                postId.put("numligne", lignesMap.get(value).getNumligne());
                            else
                                postId.put("numligne", l.getNumligne());
                            postData.put("ligne",postId);
                            urlString += "ligne";
                        }
                        if (radioa.isChecked()) {
                            if(a == null)
                                postId.put("idarret", arretsMap.get(value).getIdarret());
                            else
                                postId.put("idarret", a.getIdarret());
                            postData.put("arret", postId);
                            urlString += "arret";
                        }
                        if (radios.isChecked()) {
                            if(s == null)
                                postId.put("idstationplus", velosPMap.get(value).getIdstationplus());
                            else
                                postId.put("idstationplus", s.getIdstationplus());
                            postData.put("stationveloplus",postId);
                            urlString += "stationplus";

                        }
                        if (radiop.isChecked()) {
                            if(p == null)
                                postId.put("idparcv", parcsMap.get(value).getIdparcv());
                            else
                                postId.put("idparcv",p.getIdparcv());
                            postData.put("parcvelo", postId);
                            urlString += "parc";

                        }
                        sendData = new SendData();
                        sendData.execute(urlString, postData.toString());
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            btn_annuler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        private String getResult(String urlString) {
            String result = null;
            try {
                URL url = new URL(urlString);
                HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(httpConnection .getInputStream()); // Stream
                result = readStream(in); // Read stream
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }


    private class SendData extends AsyncTask<String, Void, Void> {
        boolean accomplished = false;

        @Override
        protected Void doInBackground(String... params) {
            StringBuilder data = new StringBuilder();

            HttpURLConnection httpURLConnection = null;
            try {
                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
                httpURLConnection.setRequestProperty("Accept", "application/json");
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                byte[] input = params[1].getBytes("utf-8");
                os.write(input, 0, input.length);

                InputStream inputStream;
                int responseStatusCode = httpURLConnection.getResponseCode();
                int status = httpURLConnection.getResponseCode();
                if( status != HttpURLConnection.HTTP_OK ) {
                    inputStream = httpURLConnection.getErrorStream();
                    //Get more informations about the problem
                } else {
                    inputStream = httpURLConnection.getInputStream();
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }

                accomplished = true;
            } catch (Exception e) {
                e.printStackTrace();
                accomplished = false;
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void unused) {
            if(accomplished) {
                finish();
            }
        }
    }
}
