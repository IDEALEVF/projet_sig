package fr.projet.sig.m2info.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.adapter.AdapterAlerteDetails;
import fr.projet.sig.m2info.item.AlerteDetailsItem;
import fr.projet.sig.m2info.model.Alerteparc;
import fr.projet.sig.m2info.model.Alerteveloplus;
import fr.projet.sig.m2info.model.Parcvelo;
import fr.projet.sig.m2info.model.Stationveloplus;

public class ActivityParcRelai extends AppCompatActivity {

    AsyncTask<Object, Void, Void> inBackground;
    private AlertDialog dialog;
    SharedPreferences sharedPreferences;


    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_parc_velo);

        inBackground = new RunInBackground();
        inBackground.execute();
    }

    private class RunInBackground extends AsyncTask<Object, Void, Void> {

        private Parcvelo parc;

        private String readStream(InputStream is) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }
        @Override
        protected Void doInBackground(Object... objects) {
            sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
            String ip = sharedPreferences.getString("ip","");
            Intent i = getIntent();
            long id = i.getLongExtra("idParc",-1);
            String urlString = "http://"+ip+":8081/cyclable/parc?idParc="+id;
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();
            String result = getResult(urlString);
            if(result == null)
                return null;
            parc = gson.fromJson(result, Parcvelo.class);
            return null;
        }

        @Override
        protected  void onPostExecute(Void unused) {
            if(parc == null)
                return;
            TextView tv_nom_arret = findViewById(R.id.tv_nom_arret);
            tv_nom_arret.setText(String.format("Parc relai %s", parc.getNomarretparc()));

            TextView tv_adresse = findViewById(R.id.tv_adresse);
            tv_adresse.setText(String.format("%s, %s", parc.getAdresseparc(), parc.getCommune()));

            StringBuilder code = new StringBuilder(parc.getCode());
            for(int i = code.length(); i<4; ++i) {
                code.insert(0, "0");
            }

            Button btn_alerte = findViewById(R.id.btn_alerte);
            ArrayList<AlerteDetailsItem> items = new ArrayList<>();
            for (Alerteparc ap : parc.getAlerteparcs())
                items.add(new AlerteDetailsItem(ap.getDate(), ap.getTextalert()));
            btn_alerte.setText(MessageFormat.format("{0}", items.size()));
            if(items.size()> 0) {
                AdapterAlerteDetails adapterAlerte = new AdapterAlerteDetails(items);
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityParcRelai.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.popup_alerte, null);
                TextView tv_info = dialogView.findViewById(R.id.tv_info);
                tv_info.setText(parc.getNomarretparc());
                RecyclerView rv_alertes = dialogView.findViewById(R.id.rv_details);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                rv_alertes.setLayoutManager(layoutManager);
                rv_alertes.setAdapter(adapterAlerte);
                builder.setCancelable(true);
                builder.setView(dialogView);
                dialog = builder.create();
                btn_alerte.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.show();
                    }
                });
            }
            else
                btn_alerte.setVisibility(View.GONE);


            findViewById(R.id.tv_creer_alerte).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ActivityNewAlerte.class);
                    intent.putExtra("idRadio",3);
                    intent.putExtra("id",parc.getIdparcv());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                }
            });



            String ip = sharedPreferences.getString("ip","");
            double lon = getIntent().getDoubleExtra("lon",-1);
            double lat = getIntent().getDoubleExtra("lat",-1);
            String url = "http://"+ip+":8081/map/parc?id="+parc.getIdparcv()+"&long="+lon+"&lat="+lat;
            WebView webView = findViewById(R.id.webview);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(url);

        }

        private String getResult(String urlString) {
            String result = null;
            try {
                URL url = new URL(urlString);
                HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(httpConnection .getInputStream()); // Stream
                result = readStream(in); // Read stream
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }
}
