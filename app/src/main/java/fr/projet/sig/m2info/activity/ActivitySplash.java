package fr.projet.sig.m2info.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import fr.projet.sig.m2info.R;

public class ActivitySplash extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
                boolean firstCo = sharedPreferences.getBoolean("firstCo",true);
                if(firstCo)
                    startActivity(new Intent(ActivitySplash.this, AcitivityOption.class));
                else
                    startActivity(new Intent(ActivitySplash.this, ActivityMenu.class));
                finish();
            }
        },3000);
    }
}
