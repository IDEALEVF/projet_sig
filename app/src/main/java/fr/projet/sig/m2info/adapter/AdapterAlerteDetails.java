package fr.projet.sig.m2info.adapter;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.ArrayList;


import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.item.AlerteDetailsItem;

public class AdapterAlerteDetails extends RecyclerView.Adapter<AdapterAlerteDetails.ViewHolder> {
    private ArrayList<AlerteDetailsItem> items;
    private Context mContext;

    public AdapterAlerteDetails(ArrayList<AlerteDetailsItem> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from((parent.getContext())).inflate(R.layout.item_alerte_details, parent, false);
        mContext = parent.getContext();
        return new AdapterAlerteDetails.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AlerteDetailsItem alerteDetailsItem = items.get(position);
        holder.alerte.setText(alerteDetailsItem.getDetails());
        holder.date.setText(DateFormat.format("'Le' dd-MM-yyyy 'à' hh:mm:ss", alerteDetailsItem.getDate()).toString());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView date, alerte;

        public ViewHolder(View itemView) {
            super(itemView);
            this.cv = itemView.findViewById(R.id.cv);
            this.date = itemView.findViewById(R.id.tv_date);
            this.alerte = itemView.findViewById(R.id.tv_details);
        }
    }

}
