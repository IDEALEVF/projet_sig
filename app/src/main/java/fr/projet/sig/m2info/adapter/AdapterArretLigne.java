package fr.projet.sig.m2info.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.activity.ActivityArret;
import fr.projet.sig.m2info.activity.ActivityLigne;
import fr.projet.sig.m2info.activity.ActivityMap;
import fr.projet.sig.m2info.item.ArretLigneItem;

public class AdapterArretLigne extends RecyclerView.Adapter<AdapterArretLigne.ViewHolder> {

    private final ActivityLigne activity;
    private ArrayList<ArretLigneItem> items;
    private Context mContext;


    public AdapterArretLigne(ArrayList<ArretLigneItem> items, ActivityLigne activity) {
        this.items = items;
        this.activity = activity;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from((parent.getContext())).inflate(R.layout.item_arret, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ArretLigneItem arretLigneItem = items.get(position);
        String text_color = arretLigneItem.getColorText();
        if(text_color.equals("0"))
            text_color = "000000";
        holder.larret.setBackgroundColor(Color.parseColor("#"+ arretLigneItem.getColorLigne()));
        holder.direction1.setText(arretLigneItem.getDirection1());
        holder.direction1.setTextColor(Color.parseColor("#"+ text_color));
        if( !arretLigneItem.getDirection2().equals("_")) {
            holder.direction2.setText(arretLigneItem.getDirection2());
            holder.direction2.setTextColor(Color.parseColor("#"+ text_color));
        }
        else
            holder.ldirection2.setVisibility(View.GONE);
        holder.ldirections.setVisibility(View.GONE);
        holder.ligne.setText(arretLigneItem.getNomArret());
        holder.ligne.setTextColor(Color.parseColor("#"+ text_color));
        holder.ligne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.ldirections.getVisibility() == View.GONE)
                    holder.ldirections.setVisibility(View.VISIBLE);
                else
                    holder.ldirections.setVisibility(View.GONE);
            }
        });
        holder.mapDirection1.setTextColor(Color.parseColor("#"+ text_color));
        holder.mapDirection2.setTextColor(Color.parseColor("#"+ text_color));
        holder.arretInfo.setTextColor(Color.parseColor("#"+ text_color));
        holder.larret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityArret.class);
                intent.putExtra("idArret", arretLigneItem.getId());
                mContext.startActivity(intent);
            }
        });

        holder.mapDirection1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ActivityMap.class);
                intent.putExtra("id",3);
                intent.putExtra("lat", activity.getLat());
                intent.putExtra("long",activity.getLon());
                intent.putExtra("numLigne", arretLigneItem.getLigne());
                intent.putExtra("idArret", arretLigneItem.getId());
                intent.putExtra("direction", arretLigneItem.getDirection1());
                activity.startActivity(intent);
            }
        });

        holder.mapDirection2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ActivityMap.class);
                intent.putExtra("id",3);
                intent.putExtra("lat", activity.getLat());
                intent.putExtra("long",activity.getLon());
                intent.putExtra("numLigne", arretLigneItem.getLigne());
                intent.putExtra("idArret", arretLigneItem.getId());
                intent.putExtra("direction", arretLigneItem.getDirection2());
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView ligne;
        TextView direction1;
        TextView direction2;
        TextView mapDirection1;
        TextView mapDirection2;
        TextView arretInfo;
        LinearLayout larret, ldirection1, ldirection2, ldirections;


        public ViewHolder(View itemView) {
            super(itemView);
            this.cv = itemView.findViewById(R.id.cv);
            this.ligne = itemView.findViewById(R.id.tv_ligne);
            this.direction1 = itemView.findViewById(R.id.tv_direction1);
            this.direction2 = itemView.findViewById(R.id.tv_direction2);
            this.ldirections = itemView.findViewById(R.id.linear_directions);
            this.ldirection1 = itemView.findViewById(R.id.linear_direction1);
            this.ldirection2 = itemView.findViewById(R.id.linear_direction2);
            this.larret = itemView.findViewById(R.id.linearArret);
            this.mapDirection1 = itemView.findViewById(R.id.tv_1);
            this.mapDirection2 = itemView.findViewById(R.id.tv_2);
            this.arretInfo = itemView.findViewById(R.id.tv_info_arret);
        }
    }
}