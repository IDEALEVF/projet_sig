package fr.projet.sig.m2info.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.activity.ActivityLigne;
import fr.projet.sig.m2info.item.LigneItem;

public class AdapterLigne extends RecyclerView.Adapter<AdapterLigne.ViewHolder> {

    private ArrayList<LigneItem> items;
    private Context mContext;

    public AdapterLigne(ArrayList<LigneItem> items) { this.items = items; }

    @NonNull
    public AdapterLigne.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from((parent.getContext())).inflate(R.layout.item_transport, parent, false);
        mContext = parent.getContext();
        return new AdapterLigne.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final LigneItem ligneItem = items.get(position);
        String text_color = ligneItem.getText_color();
        if(text_color.equals("0"))
            text_color = "000000";
        holder.numLigne.setText(ligneItem.getNumLigne());
        holder.numLigne.setTextColor(Color.parseColor("#"+text_color));


        holder.direction.setText(String.format("%s - %s", ligneItem .getDirection1(), ligneItem .getDirection2()));
        holder.direction.setTextColor((Color.parseColor("#"+text_color)));


        holder.linearArret.setBackgroundColor(Color.parseColor("#"+ligneItem.getRoute_color()));

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityLigne.class);
                intent.putExtra("numLigne",ligneItem .getNumLigne());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView numLigne, direction;
        LinearLayout linearArret;

        public ViewHolder(View itemView) {
            super(itemView);
            this.cv = itemView.findViewById(R.id.cv);
            this.numLigne = itemView.findViewById(R.id.tv_ligne);
            this.direction = itemView.findViewById(R.id.tv_direction);
            this.linearArret = itemView.findViewById(R.id.linearArret);
        }
    }
}