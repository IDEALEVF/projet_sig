package fr.projet.sig.m2info.item;

import android.os.Parcel;
import android.os.Parcelable;

import fr.projet.sig.m2info.model.Arret;
import fr.projet.sig.m2info.model.Ligne;

public class ArretLigneItem implements Parcelable {
    private Long id;
    private String ligne;
    private String nomArret;
    private String direction1;
    private String direction2;
    private String colorLigne;
    private String colorText;


    public ArretLigneItem() {}
    public ArretLigneItem(Arret arret, Ligne ligne) {
        this.id = arret.getIdarret();
        this.ligne = ligne.getNumligne();
        this.nomArret = arret.getNomarret();
        direction1 = ligne.getDirection1();
        direction2 = ligne.getDirection2();
        colorLigne = ligne.getRoute_color();
        colorText = ligne.getText_color();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLigne() {
        return ligne;
    }

    public void setLigne(String ligne) {
        this.ligne = ligne;
    }

    public String getNomArret() {
        return nomArret;
    }

    public void setNomArret(String nomArret) {
        this.nomArret = nomArret;
    }

    public String getDirection1() {
        return direction1;
    }

    public void setDirection1(String direction1) {
        this.direction1 = direction1;
    }

    public String getDirection2() {
        return direction2;
    }

    public void setDirection2(String direction2) {
        this.direction2 = direction2;
    }

    public String getColorLigne() {
        return colorLigne;
    }

    public void setColorLigne(String colorLigne) {
        this.colorLigne = colorLigne;
    }

    public String getColorText() {
        return colorText;
    }

    public void setColorText(String colorText) {
        this.colorText = colorText;
    }

    protected ArretLigneItem(Parcel in) {
        id = in.readLong();
        ligne = in.readString();
        nomArret = in.readString();
        direction1 = in.readString();
        direction2 = in.readString();
        colorText = in.readString();
        colorLigne = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(ligne);
        dest.writeString(nomArret);
        dest.writeString(direction1);
        dest.writeString(direction2);
        dest.writeString(colorLigne);
        dest.writeString(colorText);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LigneItem> CREATOR = new Creator<LigneItem>() {
        @Override
        public LigneItem createFromParcel(Parcel in) {
            return new LigneItem(in);
        }

        @Override
        public LigneItem[] newArray(int size) {
            return new LigneItem[size];
        }
    };
}
