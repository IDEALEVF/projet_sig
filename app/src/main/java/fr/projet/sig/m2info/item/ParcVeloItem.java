package fr.projet.sig.m2info.item;

import android.os.Parcel;
import android.os.Parcelable;

import fr.projet.sig.m2info.model.Parcvelo;

public class ParcVeloItem implements Parcelable {
    private Long idparcv;
    private String nomarretparc;

    public ParcVeloItem() {
    }

    public ParcVeloItem(Parcvelo p) {
        idparcv = p.getIdparcv();
        nomarretparc = p.getNomarretparc();
    }

    protected ParcVeloItem(Parcel in) {
        if (in.readByte() == 0) {
            idparcv = null;
        } else {
            idparcv = in.readLong();
        }
        nomarretparc = in.readString();
    }



    public Long getIdparcv() {
        return idparcv;
    }

    public void setIdparcv(Long idparcv) {
        this.idparcv = idparcv;
    }

    public String getNomarretparc() {
        return nomarretparc;
    }

    public void setNomarretparc(String nomarretparc) {
        this.nomarretparc = nomarretparc;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (idparcv == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(idparcv);
        }
        dest.writeString(nomarretparc);
    }




    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ParcVeloItem> CREATOR = new Creator<ParcVeloItem>() {
        @Override
        public ParcVeloItem createFromParcel(Parcel in) {
            return new ParcVeloItem(in);
        }

        @Override
        public ParcVeloItem[] newArray(int size) {
            return new ParcVeloItem[size];
        }
    };


}
