package fr.projet.sig.m2info.model;


import java.util.Date;

public class Alerteparc {

    private Long idalerte;

    private Parcvelo parcvelo;

    private String titrealerte;

    private String type; //détérioration, retard, coupure

    private String textalert;

    private Date date;
    private int cptresolu;


    public Alerteparc() {}

    public Long getIdalerte() {
        return idalerte;
    }

    public void setIdalerte(Long idalerte) {
        this.idalerte = idalerte;
    }

    public Parcvelo getParcvelo() {
        return parcvelo;
    }

    public void setParcvelo(Parcvelo parcvelo) {
        this.parcvelo = parcvelo;
    }

    public String getTitrealerte() {
        return titrealerte;
    }

    public void setTitrealerte(String titrealerte) {
        this.titrealerte = titrealerte;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTextalert() {
        return textalert;
    }

    public void setTextalert(String textalert) {
        this.textalert = textalert;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCptresolu() {
        return cptresolu;
    }

    public void setCptresolu(int cptresolu) {
        this.cptresolu = cptresolu;
    }
}
