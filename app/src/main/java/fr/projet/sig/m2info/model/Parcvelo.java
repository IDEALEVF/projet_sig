package fr.projet.sig.m2info.model;


import java.util.ArrayList;
import java.util.Collection;


public class Parcvelo {


    private Long idparcv;

    private String code;

    private String nomarretparc;

    private String commune;

    private String adresseparc;


    private Collection<Alerteparc> alerteparcs;

    public Parcvelo() {
        this.alerteparcs = new ArrayList<Alerteparc>();
    }

    public Long getIdparcv() {
        return idparcv;
    }

    public void setIdparcv(Long idparcv) {
        this.idparcv = idparcv;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNomarretparc() {
        return nomarretparc;
    }

    public void setNomarretparc(String nomarretparc) {
        this.nomarretparc = nomarretparc;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getAdresseparc() {
        return adresseparc;
    }

    public void setAdresseparc(String adresseparc) {
        this.adresseparc = adresseparc;
    }

    public Collection<Alerteparc> getAlerteparcs() {
        return alerteparcs;
    }

    public void setAlerteparcs(Collection<Alerteparc> alerteparcs) {
        this.alerteparcs = alerteparcs;
    }
    public void addAlerteparc(Alerteparc alerteparc) {
        alerteparc.setParcvelo(this);
        this.alerteparcs.add(alerteparc);
    }
}
