package fr.projet.sig.m2info.model;


import java.util.ArrayList;
import java.util.Collection;

public class Stationveloplus {


    private Long idstationplus;
    private String commune;
    private String nomarret;
    private String adressestation;
    private int totalvelos;

    private Collection<Alerteveloplus> alerteveloplus;

    public Stationveloplus() {
        this.alerteveloplus = new ArrayList<Alerteveloplus>();
    }

    public Long getIdstationplus() {
        return idstationplus;
    }

    public void setIdstationplus(Long idstationplus) {
        this.idstationplus = idstationplus;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getNomarret() {
        return nomarret;
    }

    public void setNomarret(String nomarret) {
        this.nomarret = nomarret;
    }

    public String getAdressestation() {
        return adressestation;
    }

    public void setAdressestation(String adressestation) {
        this.adressestation = adressestation;
    }

    public int getTotalvelos() {
        return totalvelos;
    }

    public void setTotalvelos(int totalvelos) {
        this.totalvelos = totalvelos;
    }

    public Collection<Alerteveloplus> getAlerteveloplus() {
        return alerteveloplus;
    }

    public void setAlerteveloplus(Collection<Alerteveloplus> alerteveloplus) {
        this.alerteveloplus = alerteveloplus;
    }
    public void addAlerteveloplus(Alerteveloplus alerteveloplus) {
        alerteveloplus.setStationveloplus(this);
        this.alerteveloplus.add(alerteveloplus);
    }
}
